view: flat_job_log {
  sql_table_name: flat_tables.flat_job_log ;;

  dimension_group: create {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.create_time ;;
    hidden: yes
  }

  dimension: failure_message {
    type: string
    sql: ${TABLE}.failure_message ;;
    hidden: yes
  }

  dimension: failure_reason {
    type: string
    sql: ${TABLE}.failure_reason ;;
    hidden: yes
  }

  dimension: job_id {
    type: number
    sql: ${TABLE}.job_id ;;
    hidden: yes
  }

  dimension: job_status {
    type: string
    sql: ${TABLE}.job_status ;;
    hidden: yes
  }

  dimension: job_type {
    type: string
    sql: ${TABLE}.job_type ;;
    hidden: yes
  }

  dimension: order_id {
    type: string
    sql: ${TABLE}.order_id ;;
    hidden: yes
  }

  measure: count {
    type: count
    drill_fields: []
    hidden: yes
  }
}
