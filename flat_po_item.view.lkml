view: flat_po_item {
  sql_table_name: flat_tables.flat_po_item ;;

  dimension_group: allocation {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.allocation_time ;;
  }

  dimension: allocation_type {
    type: number
    sql: ${TABLE}.allocation_type ;;
  }

  dimension: build_type {
    type: number
    sql: ${TABLE}.build_type ;;
  }

  dimension: cms_sku_selling_price {
    type: number
    sql: ${TABLE}.cms_sku_selling_price ;;
  }

  dimension: cms_total_selling_price {
    type: number
    sql: ${TABLE}.cms_total_selling_price ;;
  }

  dimension: feature_value {
    type: string
    sql: ${TABLE}.feature_value ;;
  }

  dimension: id_ao {
    type: number
    sql: ${TABLE}.id_ao ;;
  }

  dimension: id_io {
    type: number
    sql: ${TABLE}.id_io ;;
  }

  dimension: id_vendor {
    type: number
    sql: ${TABLE}.id_vendor ;;
  }

  dimension: is_turnkey {
    type: number
    sql: ${TABLE}.is_turnkey ;;
  }

  dimension: mapped_id_order {
    type: number
    value_format_name: id
    sql: ${TABLE}.mapped_id_order ;;
  }

  dimension: mapped_order_type {
    type: string
    sql: ${TABLE}.mapped_order_type ;;
  }

  dimension: per_sku_cost {
    type: number
    sql: ${TABLE}.per_sku_cost ;;
  }

  dimension: po_code {
    type: string
    sql: ${TABLE}.po_code ;;
  }

  dimension_group: po_create {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.po_create_time ;;
  }

  dimension: po_extra_cost {
    type: number
    sql: ${TABLE}.po_extra_cost ;;
  }

  dimension: po_item_cost {
    type: number
    sql: ${TABLE}.po_item_cost ;;
  }

  dimension: po_item_extra_cost {
    type: number
    sql: ${TABLE}.po_item_extra_cost ;;
  }

  dimension: po_item_id {
    type: number
    sql: ${TABLE}.po_item_id ;;
  }

  dimension_group: po_item_logistics_eta {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.po_item_logistics_eta ;;
  }

  dimension: po_item_pre_tax_cost {
    type: number
    sql: ${TABLE}.po_item_pre_tax_cost ;;
  }

  dimension: po_item_sku {
    type: string
    sql: ${TABLE}.po_item_sku ;;
  }

  dimension: po_status {
    type: string
    sql: ${TABLE}.po_status ;;
  }

  dimension_group: po_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.po_update_time ;;
  }

  dimension: poitem_location_id {
    type: number
    sql: ${TABLE}.poitem_location_id ;;
  }

  dimension: poitem_location_type {
    type: number
    sql: ${TABLE}.poitem_location_type ;;
  }

  dimension: poitem_location_type_name {
    type: string
    sql: ${TABLE}.poitem_location_type_name ;;
  }

  dimension: poitem_order_id {
    type: number
    sql: ${TABLE}.poitem_order_id ;;
  }

  dimension: poitem_origin_vendor {
    type: number
    sql: ${TABLE}.poitem_origin_vendor ;;
  }

  dimension: poitem_quantity {
    type: number
    sql: ${TABLE}.poitem_quantity ;;
  }

  dimension: poitem_rawcost {
    type: number
    sql: ${TABLE}.poitem_rawcost ;;
  }

  dimension: poitem_status {
    type: number
    sql: ${TABLE}.poitem_status ;;
  }

  dimension: poitem_status_name {
    type: string
    sql: ${TABLE}.poitem_status_name ;;
  }

  dimension_group: poitem_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.poitem_update_time ;;
  }

  dimension: primary_status {
    type: string
    sql: ${TABLE}.primary_status ;;
  }

  dimension: product_category {
    type: string
    sql: ${TABLE}.product_category ;;
  }

  dimension: product_type {
    type: string
    sql: ${TABLE}.product_type ;;
  }

  dimension: purchase_order_id {
    type: number
    sql: ${TABLE}.purchase_order_id ;;
  }

  dimension: purchase_order_sku_quantity {
    type: number
    sql: ${TABLE}.purchase_order_sku_quantity ;;
  }

  dimension: ship_to_location {
    type: string
    sql: ${TABLE}.ship_to_location ;;
  }

  dimension: sku_pre_tax_cost {
    type: number
    sql: ${TABLE}.sku_pre_tax_cost ;;
  }

  dimension: sku_tax_rate {
    type: number
    sql: ${TABLE}.sku_tax_rate ;;
  }

  dimension: super_category {
    type: string
    sql: ${TABLE}.super_category ;;
  }

  dimension: vendor_city {
    type: string
    sql: ${TABLE}.vendor_city ;;
  }

  dimension: vendor_commission {
    type: number
    sql: ${TABLE}.vendor_commission ;;
  }

  dimension: vendor_name {
    type: string
    sql: ${TABLE}.vendor_name ;;
  }

  measure: count {
    type: count
    drill_fields: [vendor_name, poitem_location_type_name, poitem_status_name]
  }
}
