view: sku_customization_data {
  sql_table_name: flat_tables.sku_customization_data ;;

  dimension: code {
    type: string
    sql: ${TABLE}.code ;;
  }

  dimension: field {
    type: number
    sql: ${TABLE}.field ;;
  }

  dimension: id_product {
    type: number
    sql: ${TABLE}.id_product ;;
  }

  dimension: value {
    type: number
    sql: ${TABLE}.value ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
