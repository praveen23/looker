view: marketing_quiz_response {
  sql_table_name: flat_tables.marketing_quiz_response ;;

  dimension: balcony {
    type: number
    sql: ${TABLE}.balcony ;;
  }

  dimension: bathroom {
    type: number
    sql: ${TABLE}.bathroom ;;
  }

  dimension: bathroom_remodelling {
    type: number
    sql: ${TABLE}.bathroom_remodelling ;;
  }

  dimension: bedroom {
    type: number
    sql: ${TABLE}.bedroom ;;
  }

  dimension: beds {
    type: number
    sql: ${TABLE}.beds ;;
  }

  dimension: budget {
    type: string
    sql: ${TABLE}.budget ;;
  }

  dimension: customer_id {
    type: number
    sql: ${TABLE}.customer_id ;;
  }

  dimension: decor_items {
    type: number
    sql: ${TABLE}.decor_items ;;
  }

  dimension: dining_room {
    type: number
    sql: ${TABLE}.dining_room ;;
  }

  dimension: dining_sets {
    type: number
    sql: ${TABLE}.dining_sets ;;
  }

  dimension: electrical {
    type: number
    sql: ${TABLE}.electrical ;;
  }

  dimension: expected_move {
    type: string
    sql: ${TABLE}.expected_move ;;
  }

  dimension: expected_possession {
    type: string
    sql: ${TABLE}.expected_possession ;;
  }

  dimension: expected_start {
    type: string
    sql: ${TABLE}.expected_start ;;
  }

  dimension: false_ceiling {
    type: number
    sql: ${TABLE}.false_ceiling ;;
  }

  dimension: flooring_and_tiling {
    type: number
    sql: ${TABLE}.flooring_and_tiling ;;
  }

  dimension: furnishing {
    type: number
    sql: ${TABLE}.furnishing ;;
  }

  dimension: kitchen {
    type: number
    sql: ${TABLE}.kitchen ;;
  }

  dimension: lighting {
    type: number
    sql: ${TABLE}.lighting ;;
  }

  dimension: living_room {
    type: number
    sql: ${TABLE}.living_room ;;
  }

  dimension: modular_kitchen {
    type: number
    sql: ${TABLE}.modular_kitchen ;;
  }

  dimension: modular_storage {
    type: number
    sql: ${TABLE}.modular_storage ;;
  }

  dimension: not_applicable_civil_work {
    type: number
    sql: ${TABLE}.not_applicable_civil_work ;;
  }

  dimension: not_applicable_furniture {
    type: number
    sql: ${TABLE}.not_applicable_furniture ;;
  }

  dimension: painting {
    type: number
    sql: ${TABLE}.painting ;;
  }

  dimension: plumbing {
    type: number
    sql: ${TABLE}.plumbing ;;
  }

  dimension: property_category {
    type: string
    sql: ${TABLE}.property_category ;;
  }

  dimension: property_type {
    type: string
    sql: ${TABLE}.property_type ;;
  }

  dimension: seating {
    type: number
    sql: ${TABLE}.seating ;;
  }

  dimension: service_type {
    type: string
    sql: ${TABLE}.service_type ;;
  }

  dimension: sofas {
    type: number
    sql: ${TABLE}.sofas ;;
  }

  dimension: tables {
    type: number
    sql: ${TABLE}.tables ;;
  }

  dimension: wall_treatment {
    type: number
    sql: ${TABLE}.wall_treatment ;;
  }

  dimension: wardrobes {
    type: number
    sql: ${TABLE}.wardrobes ;;
  }

  dimension: designer_can_call{
    type: string
    sql:${TABLE}.designer_can_call;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
