view: flat_credit_transactions {
  sql_table_name: flat_tables.flat_credit_transactions ;;

  dimension: amount {
    type: number
    sql: ${TABLE}.amount ;;
  }

  dimension: audit_comment {
    type: string
    sql: ${TABLE}.audit_comment ;;
  }

  dimension: comment {
    type: string
    sql: ${TABLE}.comment ;;
  }

  dimension: customer_id {
    type: number
    sql: ${TABLE}.customer_id ;;
  }

  dimension_group: date_add {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.date_add ;;
  }

  dimension_group: date_txn {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.date_txn ;;
  }

  dimension_group: date_upd {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.date_upd ;;
  }

  dimension: id_project {
    type: number
    sql: ${TABLE}.id_project ;;
  }

  dimension: id_txn {
    type: number
    sql: ${TABLE}.id_txn ;;
  }

  dimension: pay_method {
    type: string
    sql: ${TABLE}.pay_method ;;
  }

  dimension: payment_stage {
    type: string
    sql: ${TABLE}.payment_stage ;;
  }

  dimension: status {
    type: number
    sql: ${TABLE}.status ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
