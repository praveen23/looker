view: flat_project_payments {
  sql_table_name: flat_tables.flat_project_payments ;;

  dimension: entity_id {
    type: number
    sql: ${TABLE}.entity_id ;;
    hidden: yes
  }

  dimension: entity_type {
    type: string
    sql: ${TABLE}.entity_type ;;
    hidden: yes
  }

  dimension: id_project {
    type: number
    sql: ${TABLE}.id_project ;;
    hidden: yes
  }

  dimension: project_gmv {
    type: number
    sql: ${TABLE}.project_gmv ;;
    hidden: yes
  }

  dimension: ten_percent_payment {
    type: number
    sql: ${TABLE}.ten_percent_payment ;;
    hidden: yes
  }

  dimension_group: ten_percent_payment {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.ten_percent_payment_date ;;
    hidden: yes
  }
  dimension_group: gmv_date {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.gmv_date ;;
    hidden: yes
  }

  dimension: total_paid {
    type: number
    value_format_name: id
    sql: ${TABLE}.total_paid ;;
    hidden: yes
  }

  measure: count {
    type: count
    drill_fields: []
    hidden: yes
  }
}
