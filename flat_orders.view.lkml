view: flat_orders {
  sql_table_name: flat_tables.flat_orders ;;

  dimension: boq_id {
    type: number
    sql: ${TABLE}.boq_id ;;
  }

  dimension: customer_name {
    type: string
    sql: ${TABLE}.customer_name ;;
  }

  dimension_group: customer_promise {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.customer_promise_date ;;
  }

  dimension: delivery_city {
    type: string
    sql: ${TABLE}.delivery_city ;;
  }

  dimension: id_address_delivery {
    type: number
    sql: ${TABLE}.id_address_delivery ;;
  }

  dimension: id_address_invoice {
    type: number
    sql: ${TABLE}.id_address_invoice ;;
  }

  dimension: id_customer {
    type: number
    sql: ${TABLE}.id_customer ;;
  }

  dimension: id_order {
    type: number
    sql: ${TABLE}.id_order ;;
  }

  dimension: modular_type {
    type: string
    sql: ${TABLE}.modular_type ;;
  }

  dimension: order_client {
    type: string
    sql: ${TABLE}.order_client ;;
  }

  dimension_group: order_confirmed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.order_confirmed_date ;;
  }

  dimension_group: order_created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.order_created_at ;;
  }

  dimension: order_discount {
    type: number
    sql: ${TABLE}.order_discount ;;
  }

  dimension: order_handling_fee {
    type: number
    sql: ${TABLE}.order_handling_fee ;;
  }

  dimension: order_owner {
    type: string
    sql: ${TABLE}.order_owner ;;
  }
  dimension:failure_reason{
    type: string
    sql: ${flat_job_log.failure_reason};;
  }

  dimension: order_products_price {
    type: number
    sql: ${TABLE}.order_products_price ;;
  }

  dimension: order_products_wt {
    type: number
    sql: ${TABLE}.order_products_wt ;;
  }

  dimension: order_state {
    type: string
    sql: ${TABLE}.order_state ;;
  }

  dimension_group: order_status_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.order_status_update_date ;;
  }

  dimension: order_type {
    type: number
    sql: ${TABLE}.order_type ;;
  }

  dimension: order_type_name {
    type: string
    sql: ${TABLE}.order_type_name ;;
  }

  dimension_group: order_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.order_updated_at ;;
  }

  dimension: parent_order {
    type: number
    sql: ${TABLE}.parent_order ;;
  }

  dimension: project_id {
    type: number
    sql: ${TABLE}.project_id ;;
  }

  dimension: source_item_id {
    type: number
    sql: ${TABLE}.source_item_id ;;
  }

  dimension: total_paid_real {
    type: number
    sql: ${TABLE}.total_paid_real ;;
  }

  dimension: vendor_commission {
    type: number
    sql: ${TABLE}.vendor_commission ;;
  }

  dimension: vendor_facility_id {
    type: number
    sql: ${TABLE}.vendor_facility_id ;;
  }

  dimension: vendor_facility_name {
    type: string
    sql: ${TABLE}.vendor_facility_name ;;
  }

  measure: count {
    type: count
    drill_fields: [customer_name, order_type_name, vendor_facility_name]
  }

  dimension: diff_in_min {
    case: {
      when: {
        sql: ${flat_order_history.q_to_c_min} >=0 and ${flat_order_history.q_to_c_min} <=2 ;;
        label: "0-2"
      }
      when: {
        sql: ${flat_order_history.q_to_c_min} >2 and ${flat_order_history.q_to_c_min} <=4 ;;
        label: "2-4"
      }
      when: {
        sql: ${flat_order_history.q_to_c_min} >4 and ${flat_order_history.q_to_c_min} <=6 ;;
        label: "4-6"
      }
      # possibly more when statements
      else: "6+"
    }
   # hidden: yes
  }
  dimension: c_to_po_diff {
    case: {
      when: {
        sql: ${flat_order_history.c_to_po_min} >=0 and ${flat_order_history.c_to_po_min} <=1 ;;
        label: "0-1"
      }
      when: {
        sql: ${flat_order_history.c_to_po_min} >2 and ${flat_order_history.c_to_po_min} <=4 ;;
        label: "2-4"
      }
      when: {
        sql: ${flat_order_history.c_to_po_min} >4 and ${flat_order_history.c_to_po_min} <=6 ;;
        label: "4-6"
      }
      when: {
        sql: ${flat_order_history.c_to_po_min} >6 and ${flat_order_history.c_to_po_min} <=8 ;;
        label: "6-8"
      }
      when: {
        sql: ${flat_order_history.c_to_po_min} >8 and ${flat_order_history.c_to_po_min} <=10 ;;
        label: "8-10"
      }
      when: {
        sql: ${flat_order_history.c_to_po_min} >10 and ${flat_order_history.c_to_po_min} <=12 ;;
        label: "10-12"
      }
      when: {
        sql: ${flat_order_history.c_to_po_min} >12 and ${flat_order_history.c_to_po_min} <=14 ;;
        label: "12-14"
      }
      # possibly more when statements
      else: "14+"
    }
  }

  measure: avg_queued_to_confirmed {
    type: average
    sql: ${flat_order_history.q_to_c_datediff} ;;
  }

  measure: avg_confirmed_to_po {
    type: average
    sql: ${flat_order_history.po_diff} ;;
  }
}
