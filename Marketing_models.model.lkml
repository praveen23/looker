connection: "ls_analytics"

include: "*.view.lkml"         # include all views in this project
#include: "*.dashboard.lookml"  # include all dashboards in this project

# # Select the views that should be a part of this model,
# # and define the joins that connect them together.
#
# explore: order_items {
#   join: orders {
#     relationship: many_to_one
#     sql_on: ${orders.id} = ${order_items.order_id} ;;
#   }
#
#   join: users {
#     relationship: many_to_one
#     sql_on: ${users.id} = ${orders.user_id} ;;
#   }
# }
#explore: flat_projects {}
explore: marketing_view {
  join: flat_projects {
    type: left_outer
    relationship: many_to_one
    sql_on: ${flat_projects.project_id} = ${marketing_view.project_id} ;;

  }
  join: marketing_quiz_response {
    type: left_outer
    relationship: many_to_one
    sql_on: ${marketing_view.customer_id} = ${marketing_quiz_response.customer_id} ;;
  }
}
