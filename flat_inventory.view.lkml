view: flat_inventory {
  sql_table_name: flat_tables.flat_inventory ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: allocated {
    type: number
    sql: ${TABLE}.allocated ;;
  }

  dimension: assemblable {
    type: number
    sql: ${TABLE}.assemblable ;;
  }

  dimension: at_vendor {
    type: number
    sql: ${TABLE}.at_vendor ;;
  }

  dimension: at_warehouse {
    type: number
    sql: ${TABLE}.at_warehouse ;;
  }

  dimension: available {
    type: number
    sql: ${TABLE}.available ;;
  }

  dimension_group: create {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.create_time ;;
  }

  dimension: customized {
    type: number
    sql: ${TABLE}.customized ;;
  }

  dimension: id_category {
    type: number
    sql: ${TABLE}.id_category ;;
  }

  dimension: id_product {
    type: number
    sql: ${TABLE}.id_product ;;
  }

  dimension: in_transit {
    type: number
    sql: ${TABLE}.in_transit ;;
  }

  dimension: jit {
    type: number
    sql: ${TABLE}.jit ;;
  }

  dimension: parent_category {
    type: number
    sql: ${TABLE}.parent_category ;;
  }

  dimension: product_name {
    type: string
    sql: ${TABLE}.product_name ;;
  }

  dimension: prototype {
    type: number
    sql: ${TABLE}.prototype ;;
  }

  dimension: re_order_point {
    type: number
    sql: ${TABLE}.re_order_point ;;
  }

  dimension: sku_code {
    type: string
    sql: ${TABLE}.sku_code ;;
  }

  dimension: splittable {
    type: number
    sql: ${TABLE}.splittable ;;
  }

  dimension_group: update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.update_time ;;
  }

  dimension: vendor_to_accept {
    type: number
    sql: ${TABLE}.vendor_to_accept ;;
  }

  dimension: wip {
    type: number
    sql: ${TABLE}.wip ;;
  }

  measure: count {
    type: count
    drill_fields: [id, product_name]
  }
}
