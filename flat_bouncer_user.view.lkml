view: flat_bouncer_user {
  sql_table_name: flat_tables.flat_bouncer_user ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
    hidden: yes
  }

  dimension: bouncer_id {
    type: number
    sql: ${TABLE}.bouncer_id ;;
    hidden: yes
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
    hidden: yes
  }

  dimension: dp {
    label: "DP"
    type: number
    sql: ${TABLE}.dp ;;
  }

  dimension: email {
    type: string
    sql: ${TABLE}.email ;;
    hidden: yes
  }

  dimension: is_enabled {
    type: number
    sql: ${TABLE}.is_enabled ;;
  }

  dimension: name {
    label: "Designer_name"
    type: string
    sql: ${TABLE}.name ;;
    # hidden: yes
  }

  dimension: source {
    type: string
    sql: ${TABLE}.source ;;
    hidden: yes
  }

  dimension: tags {
    type: string
    sql: ${TABLE}.tags ;;
  }

  dimension: status {
    label: "Designer_status"
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension_group: user_created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.user_created_at ;;
  }
  dimension_group: inactivation_date {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.inactivation_date ;;
  }

  dimension_group: user_last_seen {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.user_last_seen ;;
    hidden: yes
  }

  measure: count {
    type: count
    drill_fields: [id, name]

  }
  measure: sum {
    type: sum
    drill_fields: [id, name]

  }
}
