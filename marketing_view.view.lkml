view: marketing_view {
  sql_table_name: flat_tables.marketing_view ;;

  dimension: active {
    type: number
    sql: ${TABLE}.active ;;
  }

  dimension: ad_source {
    type: string
    sql: ${TABLE}.ad_source ;;
  }

  dimension: bc_done {
    type: number
    sql: ${TABLE}.bc_done ;;
  }

  dimension: brief_scope {
    type: string
    sql: ${TABLE}.brief_scope ;;
  }

  dimension: city {
    type: string
    sql: ${TABLE}.city ;;
  }

  dimension: current_stage {
    type: string
    sql: ${TABLE}.current_stage ;;
  }

  dimension: current_status {
    type: string
    sql: ${TABLE}.current_status ;;
  }

  dimension: customer_id {
    type: number
    sql: ${TABLE}.customer_id ;;
  }

  dimension: effective_lead {
    type: number
    sql: ${TABLE}.effective_lead ;;
  }

  dimension: inactive {
    type: number
    sql: ${TABLE}.inactive ;;
  }

  dimension: incoming_lead {
    type: number
    sql: ${TABLE}.incoming_lead ;;
  }

  dimension: landing_page_id {
    type: number
    sql: ${TABLE}.landing_page_id ;;
  }

  dimension_group: lead {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.lead_date ;;
  }

  dimension: lead_id {
    type: number
    sql: ${TABLE}.lead_id ;;
  }

  dimension: lead_source {
    type: string
    sql: ${TABLE}.lead_source ;;
  }

  dimension: less_than_30 {
    type: number
    sql: ${TABLE}.less_than_30 ;;
  }

  dimension: order_booked {
    type: number
    sql: ${TABLE}.order_booked ;;
  }

  dimension: pincode {
    type: string
    sql: ${TABLE}.pincode ;;
  }

  dimension: pitch_sent {
    type: number
    sql: ${TABLE}.pitch_sent ;;
  }

  dimension_group: project_created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.project_created_at ;;
  }

  dimension_group: project_created_at_ist {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.project_created_at_ist ;;
  }

  dimension: project_gmv {
    type: number
    sql: ${TABLE}.project_gmv ;;
  }

  dimension: project_id {
    type: number
    sql: ${TABLE}.project_id ;;
  }

  dimension: quiz_initiated {
    type: number
    sql: ${TABLE}.quiz_initiated ;;
  }

  dimension: quiz_submitted {
    type: number
    sql: ${TABLE}.quiz_submitted ;;
  }

  dimension: seven_to_fourteen_days {
    type: number
    sql: ${TABLE}.seven_to_fourteen_days ;;
  }

  dimension: ten_percent_payment {
    type: number
    sql: ${TABLE}.ten_percent_payment ;;
  }

  dimension: trackingid {
    type: number
    value_format_name: id
    sql: ${TABLE}.trackingid ;;
  }

  dimension: utm_adgroup {
    type: string
    sql: ${TABLE}.utm_adgroup ;;
  }

  dimension: utm_campaign {
    type: string
    sql: ${TABLE}.utm_campaign ;;
  }

  dimension: utm_campaign_name {
    type: string
    sql: ${TABLE}.utm_campaign_name ;;
  }

  dimension: utm_content {
    type: string
    sql: ${TABLE}.utm_content ;;
  }

  dimension: utm_device {
    type: string
    sql: ${TABLE}.utm_device ;;
  }

  dimension: utm_medium {
    type: string
    sql: ${TABLE}.utm_medium ;;
  }

  dimension: utm_placement {
    type: string
    sql: ${TABLE}.utm_placement ;;
  }

  dimension: utm_source {
    type: string
    sql: ${TABLE}.utm_source ;;
  }

  dimension: utm_term {
    type: string
    sql: ${TABLE}.utm_term ;;
  }

  dimension: zero_to_seven_days {
    type: number
    sql: ${TABLE}.zero_to_seven_days ;;
  }

  measure: count {
    type: count
    drill_fields: [utm_campaign_name]
  }
}
