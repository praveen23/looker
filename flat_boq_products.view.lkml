view: flat_boq_products {
  sql_table_name: flat_tables.flat_boq_products ;;

  dimension: category_sku {
    type: number
    sql: ${TABLE}.category_sku ;;
  }

  dimension: hsn_code {
    type: string
    sql: ${TABLE}.hsn_code ;;
  }

  dimension: id_customized_product {
    type: number
    sql: ${TABLE}.id_customized_product ;;
  }

  dimension: igst {
    type: number
    sql: ${TABLE}.igst ;;
  }

  dimension: primary_customized_product {
    type: string
    sql: ${TABLE}.primary_customized_product ;;
  }

  dimension: product_category {
    type: string
    sql: ${TABLE}.product_category ;;
  }

  dimension: product_name {
    type: string
    sql: ${TABLE}.product_name ;;
  }

  dimension: product_price {
    type: number
    sql: ${TABLE}.product_price ;;
  }

  dimension: product_type {
    type: string
    sql: ${TABLE}.product_type ;;
  }

  dimension: sku_code {
    type: string
    sql: ${TABLE}.sku_code ;;
  }

  dimension_group: sku_created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.sku_created_at ;;
  }

  dimension_group: sku_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.sku_updated_at ;;
  }

  dimension: super_category {
    type: string
    sql: ${TABLE}.super_category ;;
  }

  dimension: type_sku {
    type: number
    sql: ${TABLE}.type_sku ;;
  }

  dimension: vms_sla_state {
    type: string
    sql: ${TABLE}.vms_sla_state ;;
  }

  dimension: vms_sla_state_id {
    type: number
    sql: ${TABLE}.vms_sla_state_id ;;
  }

  measure: count {
    type: count
    drill_fields: [product_name]
  }
}
