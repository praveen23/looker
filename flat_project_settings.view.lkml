view: flat_project_settings {
  sql_table_name: flat_tables.flat_project_settings ;;

  dimension: cm_email {
    type: string
    sql: ${TABLE}.cm_email ;;
    hidden: yes
  }

  dimension: designer_email {
    type: string
    sql: ${TABLE}.designer_email ;;
    hidden: yes
  }

  dimension: gm_email {
    type: string
    sql: ${TABLE}.gm_email ;;
    hidden: yes
  }

  dimension: pm_email {
    type: string
    sql: ${TABLE}.pm_email ;;
    hidden: yes
  }

  dimension: primary_cm {
    type: string
    sql: ${TABLE}.primary_cm ;;
    hidden: yes
  }

  dimension: primary_cm_id {
    type: number
    sql: ${TABLE}.primary_cm_id ;;
    hidden: yes
  }

  dimension: primary_designer {
    type: string
    sql: ${TABLE}.primary_designer ;;
    hidden: yes
  }

  dimension: primary_designer_id {
    type: number
    sql: ${TABLE}.primary_designer_id ;;
    hidden: yes
  }

  dimension: primary_gm {
    type: string
    sql: ${TABLE}.primary_gm ;;
    hidden: yes
  }

  dimension: primary_gm_id {
    type: number
    sql: ${TABLE}.primary_gm_id ;;
    hidden: yes
  }

  dimension: primary_pm {
    type: string
    sql: ${TABLE}.primary_pm ;;
    hidden: yes
  }

  dimension: primary_pm_id {
    type: number
    sql: ${TABLE}.primary_pm_id ;;
    hidden: yes
  }

  dimension: project_id {
    type: number
    sql: ${TABLE}.project_id ;;
    hidden: yes
  }

  dimension: source {
    type: string
    sql: ${TABLE}.source ;;
    hidden: yes
  }

  measure: count {
    type: count
    drill_fields: []
    hidden: yes
  }
}
