view: flat_projects {
  sql_table_name: flat_tables.flat_projects ;;

  dimension: address {
    type: string
    sql: ${TABLE}.address ;;
    hidden: yes
  }

  dimension_group: awaiting_10_percent {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.awaiting_10_percent_date ;;
    hidden: yes
  }

  dimension: brief_scope {
    type: string
    sql: ${TABLE}.brief_scope ;;
    #hidden: yes
  }

  dimension_group: briefing_done {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.briefing_done_date ;;
  }

  dimension: budget_max {
    type: number
    sql: ${TABLE}.budget_max ;;
  }

  dimension: budget_min {
    type: number
    sql: ${TABLE}.budget_min ;;
  }

  dimension: customer_display_name {
    type: string
    sql: ${TABLE}.customer_display_name ;;
    hidden: yes
  }

  dimension: customer_email {
    type: string
    sql: ${TABLE}.customer_email ;;
    hidden: yes
  }

  dimension: customer_phone {
    type: string
    sql: ${TABLE}.customer_phone ;;
    hidden: yes
  }

  dimension_group: design_in_progress {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.design_in_progress ;;
    hidden: yes
  }

  dimension: design_manager {
    type: string
    sql: ${TABLE}.design_manager ;;
    hidden: yes
  }

  dimension: design_manager_email {
    type: string
    sql: ${TABLE}.design_manager_email ;;
    hidden: yes
  }

  dimension: fifty_percent_amount {
    type: number
    sql: ${TABLE}.fifty_percent_amount ;;
    hidden: yes
  }

  dimension_group: fifty_percent_collected {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.fifty_percent_collected_date ;;
    #hidden: yes
  }

  dimension: full_amount {
    type: number
    sql: ${TABLE}.full_amount ;;
    hidden: yes
  }

  dimension: general_manager {
    type: string
    sql: ${TABLE}.general_manager ;;
    hidden: yes
  }

  dimension: general_manager_email {
    type: string
    sql: ${TABLE}.general_manager_email ;;
    hidden: yes
  }

  dimension: inactivation_reason {
    type: string
    sql: ${TABLE}.inactivation_reason ;;
  }

  dimension: is_awaiting_ten_percent {
    type: number
    sql: ${TABLE}.is_awaiting_ten_percent ;;
    hidden: yes
  }

  dimension: is_converted {
    type: number
    sql: ${TABLE}.is_converted ;;
  }

  dimension: is_proposal_present {
    type: number
    sql: ${TABLE}.is_proposal_present ;;
  }

  dimension: is_qualified {
    type: number
    sql: ${TABLE}.is_qualified ;;
  }

  dimension: is_test_project {
    type: number
    sql: ${TABLE}.is_test_project ;;
    #hidden: yes
  }

  dimension: last_note {
    type: string
    sql: ${TABLE}.last_note ;;
    hidden: yes
  }

  dimension: lead_medium_id {
    type: number
    sql: ${TABLE}.lead_medium_id ;;
    hidden: yes
  }

  dimension: lead_medium_name {
    type: string
    sql: ${TABLE}.lead_medium_name ;;
    #hidden: yes
  }

  dimension: lead_source {
    type: string
    sql: ${TABLE}.lead_source ;;
  }

  dimension_group: order_confirmed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.order_confirmed_date ;;
   # hidden: yes
  }

  dimension: parent_city {
    type: string
    sql: ${TABLE}.parent_city ;;
    hidden: yes
  }

  dimension: postcode {
    type: string
    sql: ${TABLE}.postcode ;;
    hidden: yes
  }

  dimension: primary_cm {
    type: string
    sql: ${TABLE}.primary_cm ;;
  }

  dimension: primary_cm_email {
    type: string
    sql: ${TABLE}.primary_cm_email ;;
    hidden: yes
  }

  dimension: primary_designer {
    type: string
    sql: ${TABLE}.primary_designer ;;
  }

  dimension: primary_gm {
    type: string
    sql: ${TABLE}.primary_gm ;;
  }

  dimension: priority {
    type: string
    sql: ${TABLE}.priority ;;
  }

  dimension: project_city {
    type: string
    sql: ${TABLE}.project_city ;;
  }

  dimension_group: project_created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.project_created_at ;;
  }

  dimension: project_created_id {
    type: number
    sql: ${TABLE}.project_created_id ;;
    hidden: yes
  }

  dimension: project_designer {
    type: string
    sql: ${TABLE}.project_designer ;;
    hidden: yes
  }

  dimension: project_designer_email {
    type: string
    sql: ${TABLE}.project_designer_email ;;
    hidden: yes
  }

  dimension: project_display_name {
    type: string
    sql: ${TABLE}.project_display_name ;;
    hidden: yes
  }

  dimension: project_id {
    type: number
    sql: ${TABLE}.project_id ;;
    primary_key: yes
  }

  dimension: project_manager {
    type: string
    sql: ${TABLE}.project_manager ;;
    hidden: yes
  }

  dimension: project_manager_email {
    type: string
    sql: ${TABLE}.project_manager_email ;;
    hidden: yes
  }

  dimension: project_pincode {
    type: string
    sql: ${TABLE}.project_pincode ;;
    hidden: yes
  }

  dimension: project_property_name {
    type: string
    sql: ${TABLE}.project_property_name ;;
    hidden: yes
  }

  dimension: project_property_category {
    type: string
    sql: ${TABLE}.project_property_category ;;
  }

  dimension: project_property_type {
    type: string
    sql: ${TABLE}.project_property_type ;;
  }

  dimension: project_service_type {
    type: string
    sql: ${TABLE}.project_service_type ;;
    #hidden: yes
  }

  dimension: project_stage {
    type: string
    sql: ${TABLE}.project_stage ;;
    hidden: yes
  }

  dimension: project_stage_weight {
    type: number
    sql: ${TABLE}.project_stage_weight ;;
    hidden: yes
  }

  dimension: project_status {
    type: string
    sql: ${TABLE}.project_status ;;
    hidden: yes
  }

  dimension_group: project_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.project_updated_at ;;
    hidden: yes
  }
  dimension: gmv_amount {
    type: number
    sql: ${TABLE}.gmv_amount ;;
    # hidden: yes
  }
#   dimension_group: gmv_date {
#     type: time
#     timeframes: [
#       raw,
#       time,
#       date,
#       week,
#       month,
#       quarter,
#       year
#     ]
#     sql: ${TABLE}.gmv_date ;;
#     # hidden: yes
#   }


  dimension: project_updated_id {
    type: number
    sql: ${TABLE}.project_updated_id ;;
    hidden: yes
  }

  dimension_group: prospective_lead {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.prospective_lead_date ;;
    hidden: yes
  }

  dimension: source {
    type: string
    sql: ${TABLE}.source ;;
  }

  dimension: status_change_reason {
    type: string
    sql: ${TABLE}.status_change_reason ;;
    hidden: yes
  }

  dimension: ten_percent_amount {
    type: number
    sql: ${TABLE}.ten_percent_amount ;;
    hidden: yes
  }

  dimension_group: ten_percent_collected {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.ten_percent_collected_date ;;
    #hidden: yes
  }

  measure: num_of_projects {
    alias: [count]
    type: count_distinct
    sql: ${project_id} ;;
    drill_fields: [project_display_name, lead_medium_name, project_property_name, customer_display_name]
  }

  measure: Time_to_BC {
    type: average
    sql: DATEDIFF(day, ${project_created_date},${briefing_done_date}) ;;
    value_format: "0 \" Days\""
    drill_fields: []
  }

  measure: Time_to_Pitch {
    type: average
    sql: DATEDIFF(day, ${project_created_date},${awaiting_10_percent_date}) ;;
    value_format: "0 \" Days\""
    drill_fields: []
  }

  measure: Time_to_ten_conversion {
    type: average
    sql: DATEDIFF(day, ${project_created_date},${ten_percent_collected_date}) ;;
    value_format: "0 \" Days\""
    drill_fields: []
  }

  measure: Time_to_first_order {
    type: average
    sql: DATEDIFF(day, ${project_created_date},${fifty_percent_collected_date}) ;;
    value_format: "0 \" Days\""
    drill_fields: []
  }


  dimension: 7_day_BC {
    case: {
      when: {
        sql: DATEDIFF( day, ${project_created_date}, ${briefing_done_date}) <=7 ;;
        label: "1"
      }
      else: "0"
    }
    hidden: yes
  }

  measure: 7_day_BC_sum {
    type: sum
    sql: CAST(${7_day_BC} AS integer);;
    drill_fields: []
    hidden: yes
  }

  measure:7_day_BC_rate{
    type: number
    sql: 100.000 * ${7_day_BC_sum} / ${num_of_projects} ;;
    value_format: "0.00\%"
  }

  dimension: 14_day_BC {
    case: {
      when: {
        sql:DATEDIFF( day, ${project_created_date}, ${briefing_done_date}) <=14;;
        label: "1"
      }
      else: "0"
    }
    hidden: yes
  }

  measure: 14_day_BC_sum {
    type: sum
    sql: CAST(${14_day_BC} as integer);;
    drill_fields: []
    hidden: yes
  }

  measure:14_day_BC_rate{
    type: number
    sql: 100.000 * ${14_day_BC_sum} / ${num_of_projects} ;;
    value_format: "0.00\%"
  }

  dimension: 7_day_pitch {
    case: {
      when: {
        sql: DATEDIFF( day, ${project_created_date}, ${awaiting_10_percent_date}) <=7 ;;
        label: "1"
      }
      else: "0"
    }
    hidden: yes
  }

  measure: 7_day_pitch_sum {
    type: sum
    sql: CAST(${7_day_pitch} as integer);;
    drill_fields: []
    hidden: yes
  }

  measure:7_day_pitch_rate{
    type: number
    sql: 100.000 * ${7_day_pitch_sum} / ${num_of_projects} ;;
    value_format: "0.00\%"
  }

  dimension: 14_day_pitch {
    case: {
      when: {
        sql:DATEDIFF( day, ${project_created_date}, ${awaiting_10_percent_date}) <=14 ;;
        label: "1"
      }
      else: "0"
    }
    hidden: yes
  }

  measure: 14_day_pitch_sum {
    type: sum
    sql: CAST(${14_day_pitch} as integer);;
    drill_fields: []
    hidden: yes
  }

  measure:14_day_pitch_rate{
    type: number
    sql: 100.000 * ${14_day_pitch_sum} / ${num_of_projects} ;;
    value_format: "0.00\%"
  }

  dimension: 21_day_pitch {
    case: {
      when: {
        sql:DATEDIFF( day, ${project_created_date}, ${awaiting_10_percent_date}) <=21 ;;
        label: "1"
      }
      else: "0"
    }
    hidden: yes
  }

  measure: 21_day_pitch_sum {
    type: sum
    sql: CAST(${21_day_pitch} as integer);;
    drill_fields: []
    hidden: yes
  }

  measure:21_day_pitch_rate{
    type: number
    sql: 100.000 * ${21_day_pitch_sum} / ${num_of_projects} ;;
    value_format: "0.00\%"
  }

  dimension: 14_day_conversion {
    case: {
      when: {
        sql: DATEDIFF( day, ${project_created_date}, ${ten_percent_collected_date}) <=14 ;;
        label: "1"
      }
      else: "0"
    }
    hidden: yes
  }

  measure: 14_day_conversion_sum {
    type: sum
    sql: CAST(${14_day_conversion} as integer);;
    drill_fields: []
    hidden: yes
  }

  measure:14_day_conversion_rate{
    type: number
    sql: 100.000 * ${14_day_conversion_sum} / ${num_of_projects} ;;
    value_format: "0.00\%"
  }

  dimension: 21_day_conversion {
    case: {
      when: {
        sql:DATEDIFF( day, ${project_created_date}, ${ten_percent_collected_date}) <=21 ;;
        label: "1"
      }
      else: "0"
    }
    hidden: yes
  }

  measure: 21_day_conversion_sum {
    type: sum
    sql: CAST(${21_day_conversion} as integer);;
    drill_fields: []
    hidden: yes
  }

  measure:21_day_conversion_rate{
    type: number
    sql: 100.000 * ${21_day_conversion_sum} / ${num_of_projects} ;;
    value_format: "0.00\%"
  }

  dimension: 30_day_conversion {
    case: {
      when: {
        sql:DATEDIFF( day, ${project_created_date}, ${ten_percent_collected_date}) <=30 ;;
        label: "1"
      }
      else: "0"
    }
    hidden: yes
  }

  measure: 30_day_conversion_sum {
    type: sum
    sql: CAST(${30_day_conversion} as integer);;
    drill_fields: []
    hidden: yes
  }

  measure:30_day_conversion_rate{
    type: number
    sql: 100.000 * ${30_day_conversion_sum} / ${num_of_projects} ;;
    value_format: "0.00\%"
  }

  dimension: 45_day_conversion {
    case: {
      when: {
        sql:DATEDIFF( day, ${project_created_date}, ${ten_percent_collected_date}) <=45 ;;
        label: "1"
      }
      else: "0"
    }
    hidden: yes
  }

  measure: 45_day_conversion_sum {
    type: sum
    sql: CAST(${45_day_conversion} as integer);;
    drill_fields: []
    hidden: yes
  }

  measure:45_day_conversion_rate{
    type: number
    sql: 100.000 * ${45_day_conversion_sum} / ${num_of_projects} ;;
    value_format: "0.00\%"
  }

  dimension: 60_day_conversion {
    case: {
      when: {
        sql:DATEDIFF( day, ${project_created_date}, ${ten_percent_collected_date}) <=60 ;;
        label: "1"
      }
      else: "0"
    }
    hidden: yes
  }

  measure: 60_day_conversion_sum {
    type: sum
    sql: CAST(${60_day_conversion} as integer);;
    drill_fields: []
    hidden: yes
  }

  measure:60_day_conversion_rate{
    type: number
    sql: 100.000 * ${60_day_conversion_sum} / ${num_of_projects} ;;
    value_format: "0.00\%"
  }
  measure: BC_Done {
    type: sum
    sql: ${is_qualified};;
    drill_fields: []
    #hidden: yes
  }

  measure:  BC_rate{
    type: number
    sql: 100.000 * ${BC_Done} / ${num_of_projects} ;;
    value_format: "0.00\%"
  }

  measure: Pitch_Presented {
    type: sum
    sql: ${is_proposal_present};;
    drill_fields: []
    #hidden: yes
  }
  measure: pitch_rate {
    type: number
    sql: 100.000 * ${Pitch_Presented} / NULLIf(${BC_Done},0) ;;
    value_format: "0.00\%"
  }

  measure: converted_Count {
    type: sum
    sql: ${is_converted};;
    drill_fields: []
    hidden: yes
  }

  measure: conversion_rate {
    type: number
    sql: 100.000 * ${converted_Count} / NULLIf(${num_of_projects},0) ;;
    value_format: "0.00\%"
  }

  measure: conversion_per_bc {
    type: number
    sql: 100.000 * ${converted_Count} / NULLIf(${BC_Done},0) ;;
    value_format: "0.00\%"
  }

  dimension: Partial_order {
    case: {
      when: {
        sql: ${fifty_percent_collected_date} is not null;;
        label: "1"
      }
      else: "0"
    }
    hidden: yes
  }

  measure: Partial_order_confirmed {
    type: sum
    sql: CAST(${Partial_order} as integer);;
    drill_fields: []
    # hidden: yes
  }

  dimension: Project_booked_case {
    case: {
      when: {
        sql: ${ten_percent_collected_date} is not null;;
        label: "1"
      }
      else: "0"
    }
    hidden: yes
  }

  measure: Project_booked {
    type: sum
    sql: CAST(${Project_booked_case} as integer);;
    drill_fields: []
    # hidden: yes
  }

  measure: total_gmv {
    type: sum
    sql: ${gmv_amount};;
    drill_fields: []
  }
  dimension: effective_leads {
    case: {
      when: {
        sql:${project_stage_weight} >=120 ;;
        label: "1"
      }
      else: "0"
    }
    hidden: yes
  }
  measure: effective_lead_count {
    type: sum
    sql: ${effective_leads};;
    drill_fields: []
    #hidden: yes
  }
  dimension: qualified_leads {
    case: {
      when: {
        sql:${project_stage_weight} >=250 ;;
        label: "1"
      }
      else: "0"
    }
    hidden: yes
  }
  measure: qualified_lead_count {
    type: sum
    sql: ${qualified_leads};;
    drill_fields: []
    #hidden: yes
  }
  dimension: proposal_presented {
    case: {
      when: {
        sql:${project_stage_weight} >=270 ;;
        label: "1"
      }
      else: "0"
    }
    hidden: yes
  }
  measure: proposal_presented_count {
    type: sum
    sql: ${proposal_presented};;
    drill_fields: []
    #hidden: yes
  }
  dimension: ps_assigned {
    case: {
      when: {
        sql:${project_stage_weight} >=125 ;;
        label: "1"
      }
      else: "0"
    }
    hidden: yes
  }
  measure: ps_assigned_count {
    type: sum
    sql: ${ps_assigned};;
    drill_fields: []
    #hidden: yes
  }
  dimension: BC_scheduled {
    case: {
      when: {
        sql:${project_stage_weight} >=130 ;;
        label: "1"
      }
      else: "0"
    }
    hidden: yes
  }
  measure: BC_scheduled_count {
    type: sum
    sql: ${BC_scheduled};;
    drill_fields: []
    #hidden: yes
  }
  dimension: converted_leads {
    case: {
      when: {
        sql:${project_stage_weight} >=400 ;;
        label: "1"
      }
      else: "0"
    }
    hidden: yes
  }
  measure: converted_leads_count {
    type: sum
    sql: ${converted_leads};;
    drill_fields: [project_id,primary_gm,primary_cm,primary_designer]
    #hidden: yes
  }
  measure: qualified_percentage {
    type: number
    sql: 100.000 * ${qualified_lead_count} / NULLIf(${effective_lead_count},0) ;;
    value_format: "0.00\%"
  }
  measure: proposal_presented_percentage {
    type: number
    sql: 100.000 * ${proposal_presented_count} / NULLIf(${qualified_lead_count},0) ;;
    value_format: "0.00\%"
  }
  measure: converted_percentage {
    label: "converted_percentage(effective)"
    type: number
    sql: 100.000 * ${converted_leads_count} / NULLIf(${effective_lead_count},0) ;;
    value_format: "0.00\%"
  }
  measure: converted_percentage_on_qualified {
    label: " converted_percentage(qualified)"
    type: number
    sql: 100.000 * ${converted_leads_count} / NULLIf(${qualified_lead_count},0) ;;
    value_format: "0.00\%"
  }
  measure: converted_percentage_on_proposal {
    label: "converted_percentage(proposal)"
    type: number
    sql: 100.000 * ${converted_leads_count} / NULLIf(${proposal_presented_count},0) ;;
    value_format: "0.00\%"
  }
  dimension_group: proposal_presented_date {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.proposal_presented_date ;;
    #hidden: yes
  }

  dimension_group: incoming_lead_date {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.incoming_lead_date ;;
    #hidden: yes
  }

  dimension_group: gm_assigned_date {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.gm_assigned_date ;;
    #hidden: yes
  }

  dimension_group: cm_assigned_date {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.cm_assigned_date ;;
    #hidden: yes
  }

  dimension_group: ps_assigned_date {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.ps_assigned_date ;;
    #hidden: yes
  }

  dimension_group: bc_scheduled_date {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.bc_scheduled_date ;;
    #hidden: yes
  }

  dimension_group: handover_with_snags_date {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.handover_with_snags_date ;;
    #hidden: yes
  }

  dimension_group: handover_with_no_snags_date {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.handover_with_no_snags_date ;;
    #hidden: yes
  }

  dimension_group: order_closed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.order_closed ;;
    #hidden: yes
  }
  dimension: stage_id {
    type: number
    sql: ${TABLE}.stage_id ;;
  }

  dimension: city_id {
    type: number
    sql: ${TABLE}.city_id ;;
  }

  dimension: customer_id {
    type: number
    sql: ${TABLE}.customer_id ;;
  }
  measure: BriefingDone_to_ProposalPresented {
    type: average
    sql: datediff(day,${briefing_done_date},${proposal_presented_date_date}) ;;
    value_format: "0.00 \" Days\""
    #drill_fields: [project_id,briefing_done_date,proposal_presented_date_date]
  }
  measure: ProposalPresented_to_DIP {
    type:  average
    sql: Datediff(day,${proposal_presented_date_date},${design_in_progress_date}) ;;
    value_format: "0.00 \" Days\""
    #drill_fields: [project_id,proposal_presented_date_date,design_in_progress_date]
  }

  measure: DIP_to_POC {
    type: average
    sql: datediff(day,${design_in_progress_date},${fifty_percent_collected_date}) ;;
    value_format: "0.00 \" Days\""
    #drill_fields: [project_id,design_in_progress_date,fifty_percent_collected_date]
  }
  measure: DIP_to_FOC {
    type: average
    sql: datediff(day,${design_in_progress_date},${order_confirmed_date}) ;;
    value_format: "0.00 \" Days\""
    #drill_fields: [project_id,design_in_progress_date,order_confirmed_date]
  }
  dimension: Handover {
    case: {
      when: {
        sql:${project_stage_weight} >=650 ;;
        label: "1"
      }
      else: "0"
    }
    hidden: no
  }
  measure: Handover_count {
    type: sum
    sql: ${Handover};;
    #drill_fields: [project_id,project_city,project_created_date,handover_with_snags_date_date,handover_with_no_snags_date_date]
    hidden: no
  }
  dimension: po_value {
    type: number
    sql: ${TABLE}.po_value ;;
     hidden: no
  }
  dimension: discount {
    type: number
    sql: ${TABLE}.discount ;;
     hidden: no
  }
  dimension: amount_paid {
    type: number
    sql: ${TABLE}.amount_paid ;;
     hidden: no
  }
  dimension: amount_pending {
    type: number
    sql: ${TABLE}.amount_pending ;;
    hidden: no
  }
  measure: total_po_value {
    type: sum
    sql: ${po_value};;
    drill_fields: [project_id,project_created_date,project_stage,project_status,gmv_amount,po_value]
  }
  measure: total_discount {
    type: sum
    sql: ${discount};;
    drill_fields: [project_id,project_created_date,project_stage,project_status,gmv_amount,po_value,discount]
  }
  measure: total_amount_paid {
    type: sum
    sql: ${amount_paid};;
    drill_fields: [project_id,project_created_date,project_stage,project_status,gmv_amount,po_value,discount,amount_paid]
  }
  measure: total_amount_pending {
    type: sum
    sql: ${amount_pending};;
    drill_fields: [project_id,project_created_date,project_stage,project_status,gmv_amount,po_value,discount,amount_paid,amount_pending]
  }
}
