view: flat_order_history {
  sql_table_name: flat_tables.flat_order_history ;;

  dimension: id_order {
    type: number
    sql: ${TABLE}.id_order ;;
    hidden: yes
  }

  dimension_group: order_confirmed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.order_confirmed_date ;;
    hidden: yes
  }

  dimension_group: order_queued {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.order_queued_date ;;
    hidden: yes
  }

  dimension_group: po_created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.po_created_at ;;
    hidden: yes
  }
  dimension: q_to_c_datediff {
    type: number
    sql: DATEDIFF(hours,${order_queued_date},${order_confirmed_date}) ;;
    hidden: yes
  }
  dimension: q_to_c_min {
    type: number
    sql: DATEDIFF(minutes,${order_queued_date},${order_confirmed_date}) ;;
    hidden: yes
  }

  dimension: po_date {
    type: date_time
    sql: ${TABLE}.po_created_at ;;
    hidden: yes
  }
  dimension: po_diff {
    type: number
    sql: DATEDIFF(hours,${order_confirmed_date},cast(${po_date} as date)) ;;
    hidden: yes
  }
  dimension: c_to_po_min {
    type: number
    sql: DATEDIFF(day,${order_confirmed_date},cast(${po_date} as date))  ;;
    hidden: yes
  }

  measure: count {
    type: count
    drill_fields: []
    hidden: yes
  }
}
