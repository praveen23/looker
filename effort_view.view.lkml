view: effort_view {
  derived_table: {
    sql: Select
        p.project_id,
        'Incoming_leads' as incoming_leads,
        p.project_created_at as incoming_month
  From
      flat_tables.flat_projects as p
--  left join
--      (
--          select
--              distinct pe.project_id,
--              min(pe.event_date) as created_at
--          from
--              flat_tables.flat_project_events as pe
--          where
--              event_type in ('STAGE_UPDATED')
--          and
--              coalesce(new_value)::INTEGER in (1,17,18,13,2,8,9,19, 11, 20, 21, 12, 3,  4,  7,  5,  14, 6,  15, 16, 10)
--          group by 1
      --)as pe1 on pe1.project_id = p.project_id
--where to_date(p.project_created_at,'YYYY-MM-DD') >= '2018-04-01'
  union
Select
        p.project_id,
        'ps_assigned' as incoming_leads,
        pe1.created_at as incoming_month
  From
      flat_tables.flat_projects as p
  left join
      (
          select
              distinct pe.project_id,
              min(pe.event_date) as created_at
          from
              flat_tables.flat_project_events as pe
          where
              event_type in ('STAGE_UPDATED')
          and
              coalesce(new_value)::INTEGER in (22,23,13,2,8,9,19,11,  20, 21, 12, 3,  4,  7,  5,  14, 6,  15, 16, 10)
          group by 1
      )as pe1 on pe1.project_id = p.project_id
union
    Select
        p.project_id,
        'effective_leads' as incoming_leads,
        pe1.created_at as incoming_month
  From
      flat_tables.flat_projects as p
  left join
      (
          select
              distinct pe.project_id,
              min(pe.event_date) as created_at
          from
              flat_tables.flat_project_events as pe
          where
              event_type in ('STAGE_UPDATED')
          and
              coalesce(new_value)::INTEGER in (17,18,13,2,8,9,19,11,20,21,12,3,4,7,5,14,6,15,16,10)
          group by 1
      )as pe1 on pe1.project_id = p.project_id
--where to_date(pe1.created_at,'YYYY-MM-DD') >= '2018-04-01'

union
  Select
        p.project_id,
        'BC_scheduled' as incoming_leads,
        pe1.created_at as incoming_month
  From
      flat_tables.flat_projects as p
  left join
      (
          select
              distinct pe.project_id,
              min(pe.event_date) as created_at
          from
              flat_tables.flat_project_events as pe
          where
              event_type in ('STAGE_UPDATED')
          and
              coalesce(new_value)::INTEGER in (23,13,2,8,9,19,11, 20, 21, 12, 3,  4,  7,  5,  14, 6,  15, 16, 10)
          group by 1
      )as pe1 on pe1.project_id = p.project_id
  --where to_date(pe1.created_at,'YYYY-MM-DD') >= '2018-04-01'

union
  Select
        p.project_id,
        'qualified_leads' as incoming_leads,
        pe1.created_at as incoming_month
  From
      flat_tables.flat_projects as p
  left join
      (
          select
              distinct pe.project_id,
              min(pe.event_date) as created_at
          from
              flat_tables.flat_project_events as pe
          where
              event_type in ('STAGE_UPDATED')
          and
              coalesce(new_value)::INTEGER in (9,19,11, 20, 21, 12, 3,  4,  7,  5,  14, 6,  15, 16, 10)
          group by 1
      )as pe1 on pe1.project_id = p.project_id


union
    Select
        p.project_id,
        'briefing_done' as incoming_leads,
        pe1.created_at as incoming_month
  From
      flat_tables.flat_projects as p
  left join
      (
          select
              distinct pe.project_id,
              min(pe.event_date) as created_at
          from
              flat_tables.flat_project_events as pe
          where
              event_type in ('STAGE_UPDATED')
          and
              coalesce(new_value)::INTEGER in (9,19,11, 20, 21, 12, 3,  4,  7,  5,  14, 6,  15, 16, 10)
          group by 1
      )as pe1 on pe1.project_id = p.project_id
--where to_date(pe1.created_at,'YYYY-MM-DD') >= '2018-04-01'
union
    Select
        p.project_id,
        'proposal_presented' as incoming_leads,
        pe1.created_at as incoming_month
  From
      flat_tables.flat_projects as p
  left join
      (
          select
              distinct pe.project_id,
              min(pe.event_date) as created_at
          from
              flat_tables.flat_project_events as pe
          where
              event_type in ('STAGE_UPDATED')
          and
              coalesce(new_value)::INTEGER in (21, 12, 3,  4,  7,  5,  14, 6,  15, 16, 10)
          group by 1
      )as pe1 on pe1.project_id = p.project_id
--  where to_date(pe1.created_at,'YYYY-MM-DD') >= '2018-04-01'
union
Select
        p.project_id,
        'conversion' as incoming_leads,
        pe1.created_at as incoming_month
  From
      flat_tables.flat_projects as p
  left join
      (
          select
              distinct pe.project_id,
              min(pe.event_date) as created_at
          from
              flat_tables.flat_project_events as pe
          where
              event_type in ('STAGE_UPDATED')
          and
              coalesce(new_value)::INTEGER in (4, 7,  5,  14, 6,  15, 16, 10)
          group by 1
      )as pe1 on pe1.project_id = p.project_id
--  where to_date(pe1.created_at,'YYYY-MM-DD') >= '2018-04-01'

 -- where to_date(pe1.created_at,'YYYY-MM-DD') >= '2018-04-01'

  ;;
  }
  dimension: project_id {
    type: number
    #primary_key: yes
    sql: ${TABLE}.project_id ;;
  }
  dimension_group: incoming_month {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.incoming_month ;;
  }
  dimension: incoming_leads {
    type: string
    sql: ${TABLE}.incoming_leads ;;
  }
  measure: count {
    type: count
    hidden: yes
  }
  dimension: incoming_lead {
    case: {
      when: {
        sql:${TABLE}.incoming_leads='Incoming_leads' ;;
        label: "1"
      }
      else: "0"
    }
    hidden: yes
  }
  dimension: qualified_leads{
    case: {
      when: {
        sql:${TABLE}.incoming_leads='qualified_leads' ;;
        label: "1"
      }
      else: "0"
    }
    hidden: yes
  }
  dimension: BC_scheduled {
    case: {
      when: {
        sql:${TABLE}.incoming_leads='BC_scheduled' ;;
        label: "1"
      }
      else: "0"
    }
    hidden: yes
  }
  dimension: briefing_done {
    case: {
      when: {
        sql:${TABLE}.incoming_leads='briefing_done' ;;
        label: "1"
      }
      else: "0"
    }
    hidden: yes
  }
  dimension: conversion {
    case: {
      when: {
        sql:${TABLE}.incoming_leads='conversion' ;;
        label: "1"
      }
      else: "0"
    }
    hidden: yes
  }
  dimension: effective_leads {
    case: {
      when: {
        sql:${TABLE}.incoming_leads='effective_leads' ;;
        label: "1"
      }
      else: "0"
    }
    hidden: yes
  }
  dimension: proposal_presented {
    case: {
      when: {
        sql:${TABLE}.incoming_leads='proposal_presented' ;;
        label: "1"
      }
      else: "0"
    }
    hidden: yes
  }
  dimension: ps_assigned {
    case: {
      when: {
        sql:${TABLE}.incoming_leads='ps_assigned' ;;
        label: "1"
      }
      else: "0"
    }
    hidden: yes
  }
  measure: incoming_lead_count {
    type: sum
    sql: ${incoming_lead} ;;
  }
  measure: effective_lead_count {
    type: sum
    sql: ${effective_leads} ;;
  }
  measure: ps_assigned_lead_count {
    type: sum
    sql: ${ps_assigned} ;;
  }
  measure: bc_scheduled_lead_count {
    type: sum
    sql: ${BC_scheduled} ;;
  }
  measure: qualified_lead_count {
    type: sum
    sql: ${qualified_leads} ;;
  }
  measure: proposal_presented_lead_count {
    type: sum
    sql: ${proposal_presented} ;;
  }
  measure: converted_lead_count {
    type: sum
    sql: ${conversion} ;;
  }
  measure: qualified_percentage {
    type: number
    sql: 100.000 * ${qualified_lead_count} / NULLIf(${effective_lead_count},0) ;;
    value_format: "0.00\%"
  }
  measure: proposal_presented_percentage {
    type: number
    sql: 100.000 * ${proposal_presented_lead_count} / NULLIf(${qualified_lead_count},0) ;;
    value_format: "0.00\%"
  }
  measure: converted_percentage {
    label: "converted_percentage(effective)"
    type: number
    sql: 100.000 * ${converted_lead_count} / NULLIf(${effective_lead_count},0) ;;
    value_format: "0.00\%"
  }
  measure: converted_percentage_on_qualified {
    label: " converted_percentage(qualified)"
    type: number
    sql: 100.000 * ${converted_lead_count} / NULLIf(${qualified_lead_count},0) ;;
    value_format: "0.00\%"
  }
  measure: converted_percentage_on_proposal {
    label: "converted_percentage(proposal)"
    type: number
    sql: 100.000 * ${converted_lead_count} / NULLIf(${proposal_presented_lead_count},0) ;;
    value_format: "0.00\%"
  }
}
