view: flat_purchase_order {
  sql_table_name: flat_tables.flat_purchase_order ;;

  dimension: delivery_address {
    type: number
    sql: ${TABLE}.delivery_address ;;
  }

  dimension: id_order {
    type: number
    sql: ${TABLE}.id_order ;;
  }

  dimension: id_warehouse {
    type: number
    sql: ${TABLE}.id_warehouse ;;
  }

  dimension: invoice_address {
    type: number
    sql: ${TABLE}.invoice_address ;;
  }

  dimension: order_type {
    type: string
    sql: ${TABLE}.order_type ;;
  }

  dimension_group: original_eta {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.original_eta ;;
  }

  dimension: po_code {
    type: string
    sql: ${TABLE}.po_code ;;
  }

  dimension: po_create_time {
    type: string
    sql: ${TABLE}.po_create_time ;;
  }

  dimension_group: po_created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.po_created_at ;;
  }

  dimension: po_extra_cost {
    type: number
    sql: ${TABLE}.po_extra_cost ;;
  }

  dimension: po_id {
    type: number
    sql: ${TABLE}.po_id ;;
  }

  dimension: po_status {
    type: string
    sql: ${TABLE}.po_status ;;
  }

  dimension: revision_no {
    type: number
    sql: ${TABLE}.revision_no ;;
  }

  dimension: term_and_cond {
    type: string
    sql: ${TABLE}.term_and_cond ;;
  }

  dimension: total_cost {
    type: number
    sql: ${TABLE}.total_cost ;;
  }

  dimension: total_item_cost {
    type: number
    sql: ${TABLE}.total_item_cost ;;
  }

  dimension: vendor_city {
    type: string
    sql: ${TABLE}.vendor_city ;;
  }

  dimension: vendor_facility_id {
    type: number
    sql: ${TABLE}.vendor_facility_id ;;
  }

  dimension: vendor_name {
    type: string
    sql: ${TABLE}.vendor_name ;;
  }

  dimension: warehouse_address_id {
    type: number
    sql: ${TABLE}.warehouse_address_id ;;
  }

  dimension: warehouse_name {
    type: string
    sql: ${TABLE}.warehouse_name ;;
  }

  measure: count {
    type: count
    drill_fields: [vendor_name, warehouse_name]
  }
}
