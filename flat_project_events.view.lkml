view: flat_project_events {
  sql_table_name: flat_tables.flat_project_events ;;

  dimension: created_by_id {
    type: number
    sql: ${TABLE}.created_by_id ;;
    hidden: yes
  }

  dimension: display_name {
    type: string
    sql: ${TABLE}.display_name ;;
    hidden: yes
  }

  dimension_group: event {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.event_date ;;
    hidden: yes
  }

  dimension: event_type {
    type: string
    sql: ${TABLE}.event_type ;;
    hidden: yes
  }

  dimension: new_value {
    type: string
    sql: ${TABLE}.new_value ;;
    hidden: yes
  }

  dimension: new_value_name {
    type: string
    sql: ${TABLE}.new_value_name ;;
    hidden: yes
  }

  dimension: old_value {
    type: string
    sql: ${TABLE}.old_value ;;
    hidden: yes
  }

  dimension: old_value_name {
    type: string
    sql: ${TABLE}.old_value_name ;;
    hidden: yes
  }

  dimension: project_id {
    type: number
    sql: ${TABLE}.project_id ;;
    hidden: yes
  }

  dimension: uid {
    type: string
    sql: ${TABLE}.uid ;;
    hidden: yes
  }

  measure: count {
    type: count
    drill_fields: [old_value_name, new_value_name, display_name]
    hidden: yes
  }

}
