view: flat_boq_items_temp {
  sql_table_name: flat_tables.flat_boq_items_temp ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: actual_shipping {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.actual_shipping_date ;;
  }

  dimension: amount_paid {
    type: number
    value_format_name: id
    sql: ${TABLE}.amount_paid ;;
  }

  dimension: cost_price {
    type: number
    sql: ${TABLE}.cost_price ;;
  }

  dimension: cover_image {
    type: string
    sql: ${TABLE}.cover_image ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension_group: deleted {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.deleted_at ;;
  }

  dimension: delivery_status {
    type: string
    sql: ${TABLE}.delivery_status ;;
  }

  dimension: discount {
    type: number
    sql: ${TABLE}.discount ;;
  }

  dimension: entity_context_data {
    type: string
    sql: ${TABLE}.entity_context_data ;;
  }

  dimension_group: estimated_delivery {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.estimated_delivery_date ;;
  }

  dimension_group: final_delivery {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.final_delivery_date ;;
  }

  dimension: handling_fee {
    type: number
    sql: ${TABLE}.handling_fee ;;
  }

  dimension_group: install {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.install_date ;;
  }

  dimension: invoice_id {
    type: number
    sql: ${TABLE}.invoice_id ;;
  }

  dimension: is_group_item {
    type: number
    sql: ${TABLE}.is_group_item ;;
  }

  dimension: is_group_sku {
    type: number
    sql: ${TABLE}.is_group_sku ;;
  }

  dimension: is_item_editable {
    type: number
    sql: ${TABLE}.is_item_editable ;;
  }

  dimension: is_no_vendor {
    type: number
    sql: ${TABLE}.is_no_vendor ;;
  }

  dimension: is_nonbillable {
    type: number
    sql: ${TABLE}.is_nonbillable ;;
  }

  dimension: is_owned_item {
    type: number
    sql: ${TABLE}.is_owned_item ;;
  }

  dimension: is_validated {
    type: number
    sql: ${TABLE}.is_validated ;;
  }

  dimension: item_status {
    type: string
    sql: ${TABLE}.item_status ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: notes {
    type: string
    sql: ${TABLE}.notes ;;
  }

  dimension: owner_entity_id {
    type: string
    sql: ${TABLE}.owner_entity_id ;;
  }

  dimension: owner_entity_type {
    type: string
    sql: ${TABLE}.owner_entity_type ;;
  }

  dimension: parent_group_item {
    type: number
    sql: ${TABLE}.parent_group_item ;;
  }

  dimension: payment_status {
    type: string
    sql: ${TABLE}.payment_status ;;
  }

  dimension: po_reference_id {
    type: string
    sql: ${TABLE}.po_reference_id ;;
  }

  dimension: product_tag {
    type: string
    sql: ${TABLE}.product_tag ;;
  }

  dimension: project_id {
    type: number
    sql: ${TABLE}.project_id ;;
  }

  dimension: proposal_id {
    type: number
    sql: ${TABLE}.proposal_id ;;
  }

  dimension: purchase_order_id {
    type: number
    sql: ${TABLE}.purchase_order_id ;;
  }

  dimension: quantity {
    type: number
    sql: ${TABLE}.quantity ;;
  }

  dimension: ref_proposal_id {
    type: number
    sql: ${TABLE}.ref_proposal_id ;;
  }

  dimension: room_tag_id {
    type: number
    sql: ${TABLE}.room_tag_id ;;
  }

  dimension: selling_price {
    type: number
    sql: ${TABLE}.selling_price ;;
  }

  dimension: sku_code {
    type: string
    sql: ${TABLE}.sku_code ;;
  }

  dimension: tax {
    type: number
    sql: ${TABLE}.tax ;;
  }

  dimension: thumbnail_image {
    type: string
    sql: ${TABLE}.thumbnail_image ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: vendor_amount_paid {
    type: number
    value_format_name: id
    sql: ${TABLE}.vendor_amount_paid ;;
  }

  dimension: vendor_id {
    type: number
    sql: ${TABLE}.vendor_id ;;
  }

  measure: count {
    type: count
    drill_fields: [id, name]
  }
}
