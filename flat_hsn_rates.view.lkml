view: flat_hsn_rates {
  sql_table_name: flat_tables.flat_hsn_rates ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: cgst {
    type: number
    sql: ${TABLE}.cgst ;;
  }

  dimension: code {
    type: string
    sql: ${TABLE}.code ;;
  }

  dimension: created_by {
    type: number
    sql: ${TABLE}.created_by ;;
  }

  dimension_group: date_add {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.date_add ;;
  }

  dimension_group: date_upd {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.date_upd ;;
  }

  dimension: defaults {
    type: number
    sql: ${TABLE}.defaults ;;
  }

  dimension: deleted {
    type: number
    sql: ${TABLE}.deleted ;;
  }

  dimension: igst {
    type: number
    sql: ${TABLE}.igst ;;
  }

  dimension: max_sale_value {
    type: number
    sql: ${TABLE}.max_sale_value ;;
  }

  dimension: min_sale_value {
    type: number
    sql: ${TABLE}.min_sale_value ;;
  }

  dimension: sgst {
    type: number
    sql: ${TABLE}.sgst ;;
  }

  dimension: state_code {
    type: number
    sql: ${TABLE}.state_code ;;
  }

  dimension: updated_by {
    type: number
    sql: ${TABLE}.updated_by ;;
  }

  measure: count {
    type: count
    drill_fields: [id]
  }
}
