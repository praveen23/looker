connection: "ls_analytics"

# include all the views
include: "*.view"

datagroup: business_models_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: business_models_default_datagroup

explore: projects {
  view_name: flat_projects
  join: flat_project_events {
    type: left_outer
    relationship: many_to_one
    sql_on: ${flat_projects.project_id} = ${flat_project_events.project_id} ;;

  }
  join: flat_project_payments {
    type: left_outer
    relationship: many_to_one
    sql_on: ${flat_projects.project_id} = ${flat_project_payments.id_project} ;;
  }
   join: flat_project_settings {
    type: left_outer
    relationship: many_to_one
    sql_on: ${flat_projects.project_id} = ${flat_project_settings.project_id} ;;
  }
  join: flat_bouncer_user {
    type: inner
    relationship: many_to_one
    sql_on: ${flat_project_settings.primary_designer_id} = ${flat_bouncer_user.bouncer_id} ;;
  }
  join: effort_view {
    type: left_outer
    relationship: one_to_one
    sql_on: ${flat_projects.project_id} = ${effort_view.project_id} ;;
  }
  join: flat_credit_transactions {
    type: left_outer
    relationship: one_to_many
    sql_on: ${flat_projects.project_id} = ${flat_credit_transactions.id_project} ;;
  }
}
explore: Orders{
  view_name: flat_orders
  join: flat_order_history {
    type: left_outer
    relationship: one_to_one
    sql_on: ${flat_orders.id_order} = ${flat_order_history.id_order};;
  }
  join: flat_job_log {
    type: inner
    relationship: one_to_one
    sql_on: ${flat_orders.id_order} = ${flat_job_log.order_id} ;;
  }

 # sql_always_where: ${flat_orders.parent_order} is not null  ;;
}

# explore: marketing_view {
#   join: flat_projects {
#     type: left_outer
#     relationship: many_to_one
#     sql_on: ${flat_projects.project_id} = ${marketing_view.project_id} ;;
#
#   }
#   join: marketing_quiz_response {
#     type: left_outer
#     relationship: many_to_one
#     sql_on: ${marketing_view.customer_id} = ${marketing_quiz_response.customer_id} ;;
#   }
# }

# explore: project_events {
#   view_name: flat_project_events
# }
#
# explore: project_payments {
#   view_name: flat_project_payments
# }
explore: flat_bouncer_user{

}
